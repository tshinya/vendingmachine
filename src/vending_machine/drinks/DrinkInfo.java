package vending_machine.drinks;

public class DrinkInfo {
	
	private String drink_name;
	private int price;
	
	protected DrinkInfo(String _drink_name, int _price) {
		this.drink_name = _drink_name;
		this.price = _price;
	}
	
	protected String getName() {
		return this.drink_name;
	}
	
	protected int getPrice() {
		return this.price;
	}

}