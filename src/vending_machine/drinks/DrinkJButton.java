package vending_machine.drinks;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import vending_machine.payment.DepositJTextField;
import vending_machine.purchase_history.PurchaseHistoryJTextArea;

public class DrinkJButton extends JButton {
	
	private DrinkInfo drink_info;
	private DepositJTextField deposit_field;
	private PurchaseHistoryJTextArea history_text_area;
	private JRadioButton purchasable_button;

	protected DrinkJButton(DrinkInfo _drink_info) {
		this.setBackground(Color.GRAY);
		this.drink_info = _drink_info;
		this.setLayout(new GridLayout(2, 1));
		this.createDrinkNameJLabel();
		this.createPurchasableJRadioButton();
	}
	
	private void createDrinkNameJLabel() {
		JLabel label = new JLabel(drink_info.getName());
		label.setHorizontalAlignment(JLabel.CENTER);
		this.add(label);
	}
	
	private void createPurchasableJRadioButton() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(1, 2));		
		this.purchasable_button = new JRadioButton();
		this.purchasable_button.setEnabled(false);
		this.purchasable_button.addMouseListener(new PurchaseActionAdapter(this));
		JLabel label = new JLabel(String.valueOf(drink_info.getPrice()));
		p.add(this.purchasable_button);
		p.add(label);
		this.add(p);
	}

	protected void setDepositJTextField(DepositJTextField _deposit_field) {
		this.deposit_field = _deposit_field;
	}
	
	protected void setPurchaseHistoryJTextArea(PurchaseHistoryJTextArea _history_text_area) {
		this.history_text_area = _history_text_area;
	}
	
	protected void purchase() {
		this.history_text_area.addPurchaseHistory(this.drink_info.getName(), this.drink_info.getPrice());
		this.deposit_field.payment(this.drink_info.getPrice());
	}
	
	protected boolean isPurchasable() {
		if (this.deposit_field.toValue() >= this.drink_info.getPrice()) return true;
		else return false;
	}
	
	protected void setPurchasable() {
		this.purchasable_button.setSelected(isPurchasable());
	}
}