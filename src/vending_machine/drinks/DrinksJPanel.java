package vending_machine.drinks;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JPanel;
import vending_machine.payment.DepositJTextField;
import vending_machine.purchase_history.PurchaseHistoryJTextArea;

public class DrinksJPanel extends JPanel{

	private DepositJTextField deposit_field;
	private PurchaseHistoryJTextArea history_text_area;
	private DrinkJButton[] drinks_button;
	
	public DrinksJPanel() {
		this.setBackground(Color.WHITE);
	}
	
	public void setDepositJTextField(DepositJTextField _deposit_field) {
		this.deposit_field = _deposit_field;
	}
	
	public void setPurchaseHistoryJTextArea(PurchaseHistoryJTextArea _history_text_area) {
		this.history_text_area = _history_text_area;
	}

	public void createDrinksButton() {
		this.setLayout(new GridLayout(2, 5, 5, 5));
		drinks_button = new DrinkJButton[DrinkInfoList.LIST.length];
		int i = 0;
		
		for (DrinkInfo drink_info : DrinkInfoList.LIST) {
			drinks_button[i] = new DrinkJButton(drink_info);
			drinks_button[i].setDepositJTextField(this.deposit_field);
			drinks_button[i].setPurchaseHistoryJTextArea(this.history_text_area);
			drinks_button[i].addMouseListener(new PurchaseActionAdapter(drinks_button[i]));
			this.add(drinks_button[i]);
			i++;
		}
	}
	
	public void setPurchasable() {
		for (DrinkJButton drink_button : drinks_button) {
			drink_button.setPurchasable();
		}
	}
}
