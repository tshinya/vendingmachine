package vending_machine.drinks;

import javax.swing.JRadioButton;

public class PurchasableRadioButton extends JRadioButton {
	
	protected PurchasableRadioButton() {
		this.setEnabled(false);
	}
}
