package vending_machine.main;

import java.awt.GridLayout;
import javax.swing.JFrame;
import vending_machine.drinks.DrinksJPanel;
import vending_machine.payment.PaymentJPanel;
import vending_machine.purchase_history.PurchaseHistoryJTextArea;

public class MainJFrame extends JFrame {

	public MainJFrame() {
		this.setSize(600, 700);
		this.setLayout(new GridLayout(3, 1, 5, 5));
		
		PurchaseHistoryJTextArea history_text_area =  new PurchaseHistoryJTextArea();
		DrinksJPanel drinks_jpanel = new DrinksJPanel();
		PaymentJPanel payment_jpanel = new PaymentJPanel();

		drinks_jpanel.setPurchaseHistoryJTextArea(history_text_area);
		drinks_jpanel.setDepositJTextField(payment_jpanel.getDepositJTextField());
		drinks_jpanel.createDrinksButton();
		payment_jpanel.setDrinksJPanel(drinks_jpanel);
		
		this.add(drinks_jpanel);
		this.add(payment_jpanel);
		this.add(history_text_area);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new MainJFrame();
	}
}
