package vending_machine.payment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangeLeverActionAdapter implements ActionListener {

	private PurseJTextField purse_field;
	private DepositJTextField deposit_field;
	
	protected ChangeLeverActionAdapter(PurseJTextField _purse_field, DepositJTextField _deposit_field) {
		this.purse_field = _purse_field;
		this.deposit_field = _deposit_field;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int change = deposit_field.payBack();
		this.purse_field.payBack(change);
	}

}
