package vending_machine.payment;

import javax.swing.JButton;

import vending_machine.drinks.DrinksJPanel;

public class ChangeLeverButton extends JButton {
	
	private DrinksJPanel drinks_jpanel;
	
	protected ChangeLeverButton(PurseJTextField purse_field, DepositJTextField deposit_field) {
		this.setText("Pay back");
		this.addActionListener(new ChangeLeverActionAdapter(purse_field, deposit_field));
	}
	
}
