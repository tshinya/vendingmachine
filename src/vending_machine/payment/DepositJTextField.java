package vending_machine.payment;

import javax.swing.JTextField;
import vending_machine.drinks.DrinksJPanel;

public class DepositJTextField extends JTextField{

	private int money;
	private DrinksJPanel drinks_jpanel;
	
	protected DepositJTextField() {
		this.money = 0;
		this.setMoneyToText();
		this.setEditable(false);
	}
	
	protected void deposit(int _money) {
		this.money += _money;
		this.setMoneyToText();
	}
	
	public void payment(int spend_money) {
		this.money -= spend_money;
		this.setMoneyToText();
	}
	
	protected int payBack() {
		int _money = this.money;
		this.money= 0;
		this.setMoneyToText();
		return _money;
	}
	
	public int toValue() {
		return Integer.valueOf(this.getText());
	}
	
	private void setMoneyToText() {
		this.setText(String.valueOf(this.money));
		if (drinks_jpanel != null) drinks_jpanel.setPurchasable();
	}
	
	protected void setDrinksJPanel(DrinksJPanel _drinks_jpanel) {
		this.drinks_jpanel = _drinks_jpanel;
	}
}