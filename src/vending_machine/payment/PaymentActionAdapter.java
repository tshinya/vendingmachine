package vending_machine.payment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PaymentActionAdapter implements ActionListener {

	private PaymentJTextField payment_field;
	private PurseJTextField purse_field;
	private DepositJTextField deposit_field;
	
	protected PaymentActionAdapter(PaymentJTextField _payment_field, PurseJTextField _purse_field, DepositJTextField _deposit_field) {
		this.payment_field = _payment_field;
		this.purse_field = _purse_field;
		this.deposit_field = _deposit_field;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (!this.payment_field.isValue())  this.payment_field.toDefault();
		
		int spend_money = this.payment_field.toValue();
		if (this.purse_field.isPayable(spend_money)) {
			this.purse_field.spendMoney(spend_money);
			this.deposit_field.deposit(spend_money);
			this.payment_field.toDefault();
		}
	}

}
