package vending_machine.payment;

import java.awt.GridLayout;
import javax.swing.JPanel;
import vending_machine.drinks.DrinksJPanel;

public class PaymentJPanel extends JPanel {

	private DepositJTextField deposit_field;
	
	public PaymentJPanel() {
		this.deposit_field = new DepositJTextField();
		PurseJTextField purse_field = new PurseJTextField();
		PaymentJTextField payment_field = new PaymentJTextField(purse_field, this.deposit_field);
		ChangeLeverButton change_button = new ChangeLeverButton(purse_field, this.deposit_field);
		
		this.setLayout(new GridLayout(2, 2, 2, 2));
		this.add(purse_field);
		this.add(change_button);
		this.add(payment_field);
		this.add(this.deposit_field);
	}

	public DepositJTextField getDepositJTextField() {
		return this.deposit_field;
	}
	
	public void setDrinksJPanel(DrinksJPanel drinks_jpanel) {
		this.deposit_field.setDrinksJPanel(drinks_jpanel);
	}
}
