package vending_machine.payment;

import javax.swing.JTextField;

public class PaymentJTextField extends JTextField {
	
	protected PaymentJTextField(PurseJTextField purse_field, DepositJTextField deposit_field) {
		this.toDefault();
		this.addActionListener(new PaymentActionAdapter(this, purse_field, deposit_field));
	}
	
	protected boolean isValue() {
		if (this.toValue() < 0) return false;
		else return true;
	}
	
	protected int toValue() {
		try {
			return Integer.valueOf(this.getText());
		} catch (Exception e) {
			return -1;
		}
	}
	
	protected void toDefault() {
		this.setText("0");
	}
	
}