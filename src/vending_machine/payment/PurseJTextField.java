package vending_machine.payment;

import javax.swing.JTextField;

public class PurseJTextField extends JTextField {

	private int money;
	
	protected PurseJTextField() {
		this.money = 10000; // set defalut
		this.setMoneyToText();
		this.setEditable(false);
	}
	
	protected boolean isPayable(int spend_money) {
		if(this.money >= spend_money) return true;
		else return false;
	}
	
	protected void spendMoney(int spend_money) {
		this.money -= spend_money;
		this.setMoneyToText();
	}
	
	protected void payBack(int change) {
		this.money += change;
		this.setMoneyToText();
	}
	
	private void setMoneyToText() {
		this.setText(String.valueOf(this.money));
	}
}
