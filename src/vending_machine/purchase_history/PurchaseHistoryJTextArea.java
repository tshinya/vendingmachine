package vending_machine.purchase_history;

import java.awt.Color;
import javax.swing.JTextArea;

public class PurchaseHistoryJTextArea extends JTextArea {

	public PurchaseHistoryJTextArea() {
		this.setBackground(Color.WHITE);
		this.setEditable(false);
	}

	public void addPurchaseHistory(String drink_name, int price) {
		this.append(drink_name + " " + String.valueOf(price) + "円\n");
	}
}